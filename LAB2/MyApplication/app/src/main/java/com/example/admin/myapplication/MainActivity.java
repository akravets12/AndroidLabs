package com.example.admin.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
        implements OrderFragment.OnOrderFragmentInteractionListener {


    ResultFragment resultFragment;
    OrderFragment orderFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultFragment = (ResultFragment) getSupportFragmentManager().findFragmentById(R.id.result_fragment);
        orderFragment = (OrderFragment) getSupportFragmentManager().findFragmentById(R.id.order_fragment);

    }

    @Override
    public void onOrderClicked(int color, String name, String min, String max) {
        resultFragment.showOrder(color, name, min, max);
    }

    @Override
    public void onSomethingChanged() {
        if (resultFragment != null)
            resultFragment.hideOrder();
    }
}

package com.example.admin.myapplication;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbRangeSeekbar;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

public class MainActivity extends AppCompatActivity {

    View pickedColor, resultView, colorView;
    BubbleThumbRangeSeekbar seekbar;
    Integer color = -1, min = 0, max = 1500;
    TextView tvMin, tvMax, nameTV, priceTV;
    Button orderButton;

    EditText flowerName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pickedColor = findViewById(R.id.pickedColor);
        tvMin = findViewById(R.id.textMin);
        tvMax = findViewById(R.id.textMax);
        seekbar = findViewById(R.id.seekBar);
        orderButton = findViewById(R.id.order);
        resultView = findViewById(R.id.resultView);
        colorView = findViewById(R.id.colorView);
        priceTV = findViewById(R.id.priceTV);
        nameTV = findViewById(R.id.nameTV);
        flowerName = findViewById(R.id.editText);

        pickedColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ColorPickerDialogBuilder
                        .with(MainActivity.this)
                        .setTitle("Choose color")
                        .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                        .density(12)
                        .setPositiveButton("ok", new ColorPickerClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int selectedColor, Integer[] allColors) {
                               pickedColor.setBackgroundColor(Color.parseColor("#" + Integer.toHexString(selectedColor)));
                               color = selectedColor;
                            }
                        })
                        .build()
                        .show();
            }
        });

        seekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(minValue));
                min = minValue.intValue();
                tvMax.setText(String.valueOf(maxValue));
                max = maxValue.intValue();
            }
        });

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showResult();
            }
        });


    }
    void showResult(){
        nameTV.setText("Назва: " + flowerName.getEditableText().toString());
        colorView.setBackgroundColor(Color.parseColor("#" + Integer.toHexString(color)));
        priceTV.setText("Ціна: " + min + " - " + max);
        resultView.setVisibility(View.VISIBLE);
    }

}

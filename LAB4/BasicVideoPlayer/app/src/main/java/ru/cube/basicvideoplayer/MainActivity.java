package ru.cube.basicvideoplayer;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_VIDEO = 12345;
    public static final int REQUEST_CODE_MUSIC = 54321;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.search_video).setOnClickListener(v -> {
            Intent pickMedia = new Intent(Intent.ACTION_GET_CONTENT);
            pickMedia.setType("video/*");
            startActivityForResult(pickMedia, REQUEST_CODE_VIDEO);
        });
        findViewById(R.id.search_music).setOnClickListener(v -> {
            Intent pickMedia = new Intent(Intent.ACTION_GET_CONTENT);
            pickMedia.setType("audio/*");
            startActivityForResult(pickMedia, REQUEST_CODE_MUSIC);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_VIDEO){
            Intent intent= new Intent(this, VideoActivity.class);
            String dataString = data.getDataString();
            intent.putExtra("video", dataString);
            startActivity(intent);
        }else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_MUSIC){
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse(data.getDataString()), "audio/mp3");
            startActivity(intent);
        }
    }
}

package com.example.admin.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbRangeSeekbar;
import com.example.admin.myapplication.dao.OrderDao;
import com.example.admin.myapplication.models.Order;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.util.List;


public class OrderFragment extends Fragment {

    private View pickedColor;
    private BubbleThumbRangeSeekbar seekbar;
    private Integer color = -1, min = 10, max = 500;
    private TextView tvMin, tvMax;
    private Button orderButton;
    private Button pickFromDBButton;

    EditText flowerName;

    private OnOrderFragmentInteractionListener mListener;

    private OrderDao orderDao = App.getInstance().getOrderDatabase().orderDao();

    public OrderFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pickedColor = view.findViewById(R.id.pickedColor);
        tvMin = view.findViewById(R.id.textMin);
        tvMax = view.findViewById(R.id.textMax);
        seekbar = view.findViewById(R.id.seekBar);
        orderButton = view.findViewById(R.id.order);
        flowerName = view.findViewById(R.id.editText);
        pickFromDBButton = view.findViewById(R.id.pick_from_db);

        pickFromDBButton.setOnClickListener(v -> {
            List<Order> all = orderDao.getAll();
            mListener.onFromDBPicked(all.isEmpty()? null : all.get(0));
        });

        pickedColor.setOnClickListener(v -> ColorPickerDialogBuilder
                .with(getActivity())
                .setTitle("Choose color")
                .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                .density(12)
                .setPositiveButton("ok", (dialog, selectedColor, allColors) -> {
                    mListener.onSomethingChanged();
                    pickedColor.setBackgroundColor(Color.parseColor("#" + Integer.toHexString(selectedColor)));
                    color = selectedColor;
                })
                .build()
                .show());

        flowerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mListener.onSomethingChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        seekbar.setOnRangeSeekbarChangeListener((minValue, maxValue) -> {
            mListener.onSomethingChanged();
            tvMin.setText(String.valueOf(minValue));
            min = minValue.intValue();
            tvMax.setText(String.valueOf(maxValue));
            max = maxValue.intValue();
        });

        orderButton.setOnClickListener(v ->
                {
                    Order order = new Order();

                    order.setColor(color);
                    order.setMax(max);
                    order.setMin(min);
                    order.setName(flowerName.getText().toString());

                    List<Order> orders = orderDao.getAll();

                    if (!orders.isEmpty()){
                        orderDao.delete(orders.get(0));
                    }

                    orderDao.insert(order);

                    mListener.onOrderClicked(order);
                }
        );

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnOrderFragmentInteractionListener) {
            mListener = (OnOrderFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnOrderFragmentInteractionListener {
        void onOrderClicked(Order order);
        void onFromDBPicked(Order order);
        void onSomethingChanged();
    }
}

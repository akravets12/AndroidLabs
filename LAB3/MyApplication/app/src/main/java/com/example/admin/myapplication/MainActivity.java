package com.example.admin.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.admin.myapplication.models.Order;

public class MainActivity extends AppCompatActivity
        implements OrderFragment.OnOrderFragmentInteractionListener {


    private ResultFragment resultFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultFragment = (ResultFragment) getSupportFragmentManager().findFragmentById(R.id.result_fragment);
    }


    @Override
    public void onOrderClicked(Order order) {
        resultFragment.showOrder(order);
    }

    @Override
    public void onFromDBPicked(Order order) {
        if (order == null) {
            Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
        } else {
            CustomDialog dialog = new CustomDialog(this);
            dialog.setOrder(order);
            dialog.show();
        }
    }

    @Override
    public void onSomethingChanged() {
        if (resultFragment == null) return;
        resultFragment.hideOrder();
    }

}

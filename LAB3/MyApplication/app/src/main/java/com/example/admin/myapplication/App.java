package com.example.admin.myapplication;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.admin.myapplication.db.OrderDatabase;

public class App extends Application {

    private static App instance;

    private OrderDatabase orderDatabase;


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        orderDatabase = Room
                .databaseBuilder(this, OrderDatabase.class, "db")
                .allowMainThreadQueries()
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public OrderDatabase getOrderDatabase() {
        return orderDatabase;
    }
}

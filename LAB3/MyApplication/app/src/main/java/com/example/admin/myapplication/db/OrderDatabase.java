package com.example.admin.myapplication.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.admin.myapplication.dao.OrderDao;
import com.example.admin.myapplication.models.Order;

@Database(entities = {Order.class}, version = 1)
public abstract class OrderDatabase  extends RoomDatabase {
    public abstract OrderDao orderDao();
}

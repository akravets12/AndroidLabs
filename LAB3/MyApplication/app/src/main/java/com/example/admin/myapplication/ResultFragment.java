package com.example.admin.myapplication;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.myapplication.models.Order;


public class ResultFragment extends Fragment {


    View resultView, colorView;
    TextView nameTV;
    TextView priceTV;


    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resultView = view.findViewById(R.id.resultView);
        resultView = view.findViewById(R.id.resultView);
        colorView = view.findViewById(R.id.colorView);
        nameTV = view.findViewById(R.id.nameTV);
        priceTV = view.findViewById(R.id.priceTV);

    }

    public void showOrder(Order order){
        resultView.setVisibility(View.VISIBLE);
        nameTV.setText(getString(R.string.name) + order.getName());
        int color = Color.parseColor("#" + Integer.toHexString(order.getColor()));
        colorView.setBackgroundColor(color);
        priceTV.setText(getString(R.string.price) + order.getMin() + " - " + order.getMax());
        resultView.setVisibility(View.VISIBLE);
    }

    public void hideOrder(){
        resultView.setVisibility(View.GONE);
    }
}

package com.example.admin.myapplication;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.admin.myapplication.models.Order;

public class CustomDialog extends Dialog {

    View resultView, colorView;
    TextView nameTV;
    TextView priceTV, dismiss;

    private Order order;

    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_view);

        resultView = findViewById(R.id.resultView);
        colorView = findViewById(R.id.colorView);
        nameTV = findViewById(R.id.nameTV);
        priceTV = findViewById(R.id.priceTV);
        dismiss = findViewById(R.id.dismiss);

        dismiss.setOnClickListener(v -> dismiss());

        nameTV.setText(getContext().getString(R.string.name) + order.getName());
        int color = Color.parseColor("#" + Integer.toHexString(order.getColor()));
        colorView.setBackgroundColor(color);
        priceTV.setText(getContext().getString(R.string.price) + order.getMin() + " - " + order.getMax());
    }

    public void setOrder(Order order) {
        this.order = order;

    }
}

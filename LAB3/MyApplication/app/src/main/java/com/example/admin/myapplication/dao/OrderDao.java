package com.example.admin.myapplication.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.admin.myapplication.models.Order;

import java.util.List;

@Dao
public interface OrderDao {

    @Query("SELECT * FROM `Order`")
    List<Order> getAll();

    @Insert
    void insert(Order order);

    @Update
    void update(Order order);


    @Delete
    void delete(Order order);
}
